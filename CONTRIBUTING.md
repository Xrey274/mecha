# Contributing

## Environment details

### Mecha is designed to be C++17(and above), as it utilizes modern features of the language and it's standard library. Stick close to these three: Unix philosophy, FLOSS and KISS.


Use of the preprocessor(besides #pragma once and #include) is forbidden.

Artifacts from buildsystems(Meson, CMake, etc..) or IDEs in the form of files or directories are NOT acceptable. 

Introducing new dependecies is strongly discouraged. 

Always prefer smartpointers over raw pointers for heap allocation. 

Prefer strings over char arrays and pointers. 

Always prefer STL functions over third-party library functons or self-implementations.

Use '\n' over ::endl when working with IO streams. 

Structs are reserved for POD(plan old data).

Be const correct. Const not only prevents modification, but also signifies intention.

No singletons. 

## Code Style

Everything, unless stated otherwise, is "snake_case".

Constants are "ALLCAPS_SNAKE_CASE".

Class member variables use Hungarian notation "m_".

Brackets that belong to a function, class or similar are always on next line(does NOT apply to function calls, only defentions).

Soft maximum line length of 100 characters. There is not hard limit and if needed you are allowed to go ever 100 characters, just don't make it the norm.
Comments and function declarations are excluded from character limits.
