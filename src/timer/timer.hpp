#pragma once

#include <string>
#include <chrono>
#include <sstream>
#include <vector>
#include <cmath>
#include <iostream>

#include "time_struct.hpp"

using namespace std::chrono;

class timer
{
	public:
		timer();

		void start_timer();
		void stop_timer();
		std::string get_elapsed_time();
	private:
		mecha::time millisecond_counter;
		mecha::time second_counter;
		mecha::time minute_counter;
		mecha::time hour_counter;

		steady_clock::time_point timer_begin;
		steady_clock::time_point timer_end;
};