#include "timer.hpp"

timer::timer()
{
	millisecond_counter.amount = 0;
	millisecond_counter.unit   = "milliseconds";

	second_counter.amount      = 0;
	second_counter.unit        = "seconds";

	minute_counter.amount      = 0;
	minute_counter.unit        = "minutes";

	hour_counter.amount        = 0;
	hour_counter.unit          = "hours";
}

void
timer::start_timer()
{
	timer_begin = steady_clock::now();
}

void
timer::stop_timer()
{
	timer_end = steady_clock::now();

    double end_time   = time_point_cast<milliseconds>(timer_end).time_since_epoch().count();
    double start_time = time_point_cast<milliseconds>(timer_begin).time_since_epoch().count();

    millisecond_counter.amount = end_time - start_time;
}

std::string 
timer::get_elapsed_time()
{
    // shows if elapsed time can further be divided to form a bigger unit (ex. 1000 milliseconds -> 1 second)
    bool is_divisible = true;

    while(is_divisible)
    {
    	if(millisecond_counter.amount >= 1000)
	    {
	    	// convert milliseconds to seconds
	        double elapsed_time = millisecond_counter.amount / 1000.f;

	        // seperate integer and fraction using modf
	        double integer  = 0;
	        double fraction = modf(elapsed_time, &integer);

	        // add seconds to second_counter
	        second_counter.amount = integer;
	        // subtract the fraction
	        millisecond_counter.amount = fraction * 1000;

	        break;
	    }

	    if(second_counter.amount >= 60)
	    {
	    	// convert seconds to minutes
	        double elapsed_time = second_counter.amount / 60.f;

	        // seperate integer and fraction using modf
	        double integer  = 0;
	        double fraction = modf(elapsed_time, &integer);

	        // add seconds to second_counter
	        minute_counter.amount = integer;
	        // subtract the fraction
	        second_counter.amount = fraction * 60;

	        break;
	    }

		if(minute_counter.amount >= 60)
	    {
	    	// convert minutes to hours
	        double elapsed_time = minute_counter.amount / 60.f;

	        // seperate integer and fraction using modf
	        double integer  = 0;
	        double fraction = modf(elapsed_time, &integer);

	        // add seconds to second_counter
	        hour_counter.amount = integer;
	        // subtract the fraction
	        second_counter.amount = fraction * 60;

	        break;
	    }

	    else
	    {
	    	is_divisible = false;
	    }

    }

    std::string elapsed_time_str;

    if(hour_counter.amount > 0)
    	elapsed_time_str +=       std::to_string(hour_counter.amount)        + " " + hour_counter.unit;

    if(minute_counter.amount > 0)
    	elapsed_time_str += " " + std::to_string(minute_counter.amount)      + " " + minute_counter.unit;

    if(second_counter.amount > 0)
  		elapsed_time_str += " " + std::to_string(second_counter.amount)      + " " + second_counter.unit;

    elapsed_time_str     += " " + std::to_string(millisecond_counter.amount) + " " + millisecond_counter.unit;

    return elapsed_time_str;
}