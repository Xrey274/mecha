#include "parser.hpp"

void
parser::detect_project(const mecha::token& token)
{
	if(token.string == "project" && !current_range.empty())
	{
		mecha::token name = lex.get_token();

		if(name.string.length() < 3)
			error(name, parser_error::project_name_length);

		// project cannot have the same name as function
		for(auto& function : project_function_vector)
		{
			if(name.string == function)
				error(name, parser_error::project_name_function);
		}

		// project shouldn't contain special chars (see parser.hpp "special_chars" vector)
		for(auto& chars : special_chars_list)
		{
			if(name.string.find(chars, 0) != std::string::npos)
				error(name, parser_error::project_name_character);
		}

		mecha::project new_project{name.string};

		ranges[current_range].projects.push_back(new_project);

		throw true;
	}
	else if(token.string == "project" && current_range.empty())
		error(token, parser_error::project_outside_range);
}