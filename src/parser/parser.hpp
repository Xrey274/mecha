#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <utility>
#include <thread>
#include <cstdlib>
#include <cmath>

#include "../lexer/lexer.hpp"
#include "../common/range_struct.hpp"
#include "../common/function_struct.hpp"
#include "../common/shell_colors.hpp"
#include "../build/script_functions.hpp"
#include "../build/build.hpp"
#include "../common/token.hpp"
#include "../common/error.hpp"

class parser
{
	public:
		parser();

		void parse();

		void detect_range(const mecha::token&);
		void range_recursive_parse();

		void detect_project(const mecha::token&);
		
		void detect_project_function(const mecha::token&);
		void detect_standalone_function(const mecha::token&);
		void detect_global_function(const mecha::token&);
		void detect_function_parameters(mecha::function&);

		void push_opening_brace(const mecha::token&, parser_error);
		void push_closing_brace(const mecha::token&, parser_error);

		void push_opening_parentheses(const mecha::token&, parser_error);
		void push_closing_parentheses(const mecha::token&, parser_error);


		void build(std::string);

	private:
		lexer lex;

		std::string current_range;

		// standalone functions outside of a range
		std::vector<mecha::function> functions;
		
		std::unordered_map<std::string, mecha::range> ranges;

		std::stack<char> braces;
		std::stack<char> parentheses;

		const std::vector<char> special_chars_list
		{
			'{', '}', '(', ')', '.', ','
		};
};