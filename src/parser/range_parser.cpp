#include <parser.hpp>

void
parser::detect_range(const mecha::token& token)
{
	if(token.string == "range" && current_range.empty())
	{
		mecha::token name = lex.get_token();

		// check length of name
		if(name.string.length() < 3)
			error(name, parser_error::range_name_length);

		// check if name has a collision with function name
		for(auto& function : project_function_vector)
			if(name.string == function)
				error(name, parser_error::range_name_function);

		// check if name contains special chars
		for(auto& chars : special_chars_list)
			if(name.string.find(chars, 0) != std::string::npos)
				error(name, parser_error::range_name_character);

		push_opening_brace(lex.get_token(), parser_error::range_not_opened);

		mecha::range new_range{name.string};

		ranges.insert({name.string, new_range});

		current_range = name.string;

		range_recursive_parse();

		current_range = "";

		throw true;
	}
	else if(token.string == "range" && !current_range.empty())
	{
		error(token, parser_error::range_inside_range);
	}
}

void
parser::range_recursive_parse()
{
	// parse content of range
	while(braces.size())
	{
		try
		{
			mecha::token next_token = lex.get_token();

			detect_project(next_token);
			detect_project_function(next_token);

			push_closing_brace(next_token, parser_error::unknown_token);

			// if reached EOF
			if(lex.get_token_number() - 1 == lex.get_token_index())
				// if there's braces don't have a pair
				if(braces.size())
					error({current_range, next_token.line_number}, parser_error::range_not_closed);
		}
		catch(const bool found_match)
		{
			// stop parsing this token, if a match is found
			if(found_match)
				continue;
		}
	}
}