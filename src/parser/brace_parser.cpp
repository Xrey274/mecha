#include "parser.hpp"

void
parser::push_opening_brace(const mecha::token& token, parser_error error_code)
{
	if(token.string == "{")
	{
		braces.push('{');
	}
	else
		error(token, error_code);
}

void
parser::push_closing_brace(const mecha::token& token, parser_error error_code)
{
	if(token.string == "}")
	{
		braces.pop();

		throw true;
	}
	else
		error(token, error_code);
}