#include "parser.hpp"

void
parser::detect_project_function(const mecha::token& token)
{
	if(!current_range.empty())
	{
		for(auto& project : ranges[current_range].projects)
		{
			if(token.string == project.name)
			{
				if(lex.get_token().string == ".")
				{
					mecha::token name = lex.get_token();

					// check if name is valid
					if(project_function_map.find(name.string) == project_function_map.end())
						error(name, parser_error::unknown_function);

					mecha::function new_function{name.string, project_function_map.at(name.string)};

					detect_function_parameters(new_function);

					project.functions.push_back(new_function);

					throw true;
				}
				else
					error(token, parser_error::unknown_token_dot);
			}
		}
		// if token is not found in list of projects, leave function
		return;
	}
}

void
parser::detect_global_function(const mecha::token& token)
{
	// if function is NOT global
	if(!global_function_map.count(token.string))
	{
		// check if it's a project function
		if(project_function_map.count(token.string))
			error(token, parser_error::function_without_project);
		else
			return;
	}

	if(!current_range.empty())
		error(token, parser_error::global_function_in_range);

	mecha::function new_function{token.string, global_function_map.at(token.string)};

	detect_function_parameters(new_function);

	functions.push_back(new_function);

	throw true;
}

void
parser::detect_function_parameters(mecha::function& new_function)
{
	push_opening_parentheses(lex.get_token(), parser_error::function_not_opened);

	while(true)
	{
		// if " (first pair or "opening")
		if(lex.get_token().string == R"(")")
		{
			mecha::token parameters = lex.get_token();

			if(parameters.string.empty())
				error(parameters, parser_error::expected_parameters);

			// if " (second pair or "closing")
			if(lex.get_token().string != R"(")")
				error(lex.get_current_token(), parser_error::expected_quotes);

			new_function.parameters.push_back(parameters.string);

			if(!(lex.get_token().string == ","))
			{
				push_closing_parentheses(lex.get_current_token(), parser_error::function_not_closed);

				break;
			}
			else
				continue;
		}
		else
		{
			push_closing_parentheses(lex.get_current_token(), parser_error::function_not_closed);

			break;
		}
	}

}