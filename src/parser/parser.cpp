#include "parser.hpp"

parser::parser()
{
	check_pkg_config();
	check_libmagic();

	lex.lex();
}

void
parser::parse()
{
	unsigned int number_of_tokens = lex.get_token_number();

	if(number_of_tokens != 1)
		--number_of_tokens;

	for(std::size_t i = 0; i < number_of_tokens; i = lex.get_token_index())
	{
		try
		{
			mecha::token token = lex.get_token();

			detect_range(token);
			detect_global_function(token);

			// if it reaches here, then throw parser_error for unknown token
			error(token, parser_error::unknown_token);

			if(!braces.size())
				current_range = "";
		}
		catch(const bool found_match)
		{
			if(found_match)
				continue;
		}
	}
}

void
parser::build(std::string range_to_execute)
{
	for(mecha::function& function : functions)
	{
		switch(function.type)
		{
			case functions::default_range:
				range_to_execute = function.parameters[0];

				break;
			case functions::move:
					move(function.parameters[0], function.parameters[1]);

					break;
			case functions::copy:
					copy(function.parameters[0], function.parameters[1]);

					break;
			default:
				break;
		}
	}

	// if there's not specified ranges
	if(range_to_execute.empty())
	{
		std::string list_of_ranges;

		for(auto& range : ranges)
		{
			list_of_ranges += " " + range.first;
		}

		std::cout << "List of ranges:" << list_of_ranges << "\n";

		return;
	}

	if(!ranges.count(range_to_execute))
		error(mecha::token{range_to_execute, 0}, parser_error::range_name_unknown);

	for(mecha::project& project : ranges[range_to_execute].projects)
	{
		for(mecha::function& function : project.functions)
		{
			switch(function.type)
			{
				case functions::set_name:
					set_name(function.parameters, project.executable_name);

					break;
				case functions::add_files:
					add_files(function.parameters, project.source_files);

					break;
				case functions::add_files_recursive:
					add_files_recursive(function.parameters, project.source_files);

					break;
				case functions::pkg_config_find:
					pkg_config_find(function.parameters, project.compiler_flags, project.linker_flags);

					break;
				case functions::set_linker_flags:
					set_linker_flags(function.parameters, project.linker_flags);

					break;
				case functions::set_compiler:
					set_compiler(function.parameters, project.compiler_name);

					break;
				case functions::set_compiler_flags:
					set_compiler_flags(function.parameters, project.compiler_flags);

					break;
				case functions::output_to:
					break;
				case functions::thread_info:
					thread_info(project.source_files, get_thread_count(project.source_files));

					break;
				default:
					break;
			}
		}

		std::vector<std::thread> thread_pool;
		thread_pool.reserve(get_thread_count(project.source_files));

		compile(thread_pool, balance_load(project.source_files, project.compiler_name, project.compiler_flags));
		link(project.compiler_name, project.executable_name, project.linker_flags);
	}
}