#include "parser.hpp"

void
parser::push_opening_parentheses(const mecha::token& token, parser_error error_code)
{
	if(token.string == "(")
	{
		parentheses.push('(');
	}
	else
		error(token, error_code);
}

void
parser::push_closing_parentheses(const mecha::token& token, parser_error error_code)
{
	if(token.string == ")")
	{
		parentheses.pop();
	}
	else
		error(token, error_code);
}