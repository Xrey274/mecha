#pragma once

#include <iostream>
#include <filesystem>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>

#include "locate_script.hpp"
#include "function_struct.hpp"
#include "execute.hpp"
#include "shell_colors.hpp"

namespace fs = std::filesystem;

inline void
check_compiler(const std::string& compiler)
{
	std::string output = execute_silent("which " + compiler);

	// check if "which" returns an error
	if(output.find("which: no " + compiler) != std::string::npos)
		error(io_error::compiler_not_found);
}

inline void
check_pkg_config()
{
	std::string output = execute_silent("which pkg-config");

	// check if "which" returns an error
	if(output.find("which: no pkg-config") != std::string::npos)
		error(io_error::pkg_config_not_found);
}

inline void
check_libmagic()
{
	std::string output = execute_silent("which file");

	// check if "which" returns an error
	if(output.find("which: no file") != std::string::npos)
		error(io_error::libmagic_not_found);
}

inline void
set_name(const std::vector<std::string>& parameters, std::string& executable_name)
{
	executable_name = parameters[0];
}

inline void
set_compiler(const std::vector<std::string>& parameters, std::string& compiler_name)
{
	check_compiler(parameters[0]);

	compiler_name = parameters[0];
}

inline void
set_compiler_flags(const std::vector<std::string>& parameters, std::vector<std::string>& compiler_flags)
{
	for(const std::string& flag : parameters)
	{
		compiler_flags.push_back(flag);
	}	
}

inline void
add_files(const std::vector<std::string>& parameters, std::vector<std::string>& source_files)
{
	// example n = /home/user/program/file.cpp
	for(const std::string& file : parameters)
	{
		source_files.push_back(file);
	}
}

inline void
add_files_recursive(std::vector<std::string>& parameters, std::vector<std::string>& source_files)
{
	for(std::size_t i = 0; i < parameters.size(); ++i)
	{
		std::string current_path = parameters[i];

		for(auto entry : fs::directory_iterator(current_path))
		{
			if(entry.is_regular_file())
			{
				std::string mime_type = execute_silent("file -b --mime-type " + entry.path().string());
				std::string extension = entry.path().extension();

				if(mime_type == "text/x-c" || mime_type == "text/x-c++")
				{
					// ignore header files
					if(
						extension != ".h"   &&
						extension != ".hpp" &&
						extension != ".hh"  &&
						extension != ".H"   &&
						extension != ".HPP" &&
						extension != ".HH")
					{
						source_files.push_back(entry.path().string());
					}
				}
			}
			else if(entry.is_directory())
			{
				// add directory to parameters vector to be searched for files
				parameters.push_back(entry.path().string());
			}
		}
	}
}

inline void
pkg_config_find(const std::vector<std::string>& parameters, std::vector<std::string>& compiler_flags, std::vector<std::string>& linker_flags)
{
	for(std::string lib : parameters)
	{
		std::string error_message = "No package '" + lib + "' found";

		std::string libs   = execute_silent("pkg-config --short-errors --libs " + lib);
		std::string cflags = execute_silent("pkg-config --short-errors --cflags " + lib);
		

		if(libs != error_message)
		{
			linker_flags.push_back(libs);
		}
		else if(cflags != error_message)
		{
			compiler_flags.push_back(cflags);
		}
		else
		{
			throw error_message;
		}
	}
}

inline void
set_linker_flags(const std::vector<std::string>& parameters, std::vector<std::string>& linker_flags)
{
	for(std::string flag : parameters)
	{
		linker_flags.push_back(flag);
	}
}

inline void
thread_info(const std::vector<std::string>& source_files, int thread_count)
{
	float files_per_thread = source_files.size() / thread_count;

	std::cout << BOLD_BLUE << "File count: "       << source_files.size() << RESET << "\n"; 
	std::cout << BOLD_BLUE << "Thread count: "     << thread_count        << RESET << "\n";
	std::cout << BOLD_BLUE << "Files per thread: " << files_per_thread    << RESET << "\n";
}

inline void
move(const std::string& source_file, const std::string& destination)
{
	std::string file_name = std::filesystem::path(source_file).filename();

	std::filesystem::copy(source_file, destination + "/" + file_name);
	std::filesystem::remove(source_file);

	std::cout << BOLD_WHITE << "Moved " + source_file + " to " + destination << RESET << "\n";
}

inline void
copy(const std::string& source_file, const std::string& destination)
{
	std::cout << BOLD_WHITE << "Copied " + source_file + " to " + destination << RESET << "\n";
	std::string file_name = std::filesystem::path(source_file).filename();

	std::filesystem::copy(source_file, destination + "/" + file_name);

}