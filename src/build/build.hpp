#pragma once

#include <iostream>
#include <cstdio>
#include <string>
#include <filesystem>

#include "range_struct.hpp"
#include "project_struct.hpp"
#include "function_struct.hpp"
#include "execute.hpp"

namespace fs = std::filesystem;

inline unsigned int
get_thread_count(const std::vector<std::string>& source_files)
{
	unsigned int thread_count = 0; 

	if(source_files.size() <= std::thread::hardware_concurrency())
	{
		thread_count = source_files.size();
	}
	else
	{
		thread_count = std::thread::hardware_concurrency();
	}

	return thread_count;
}

inline std::vector<std::string>
balance_load(const std::vector<std::string>& source_files, std::string compiler_name, std::vector<std::string> compiler_flags)
{
	// average number files per thread
	float files_per_thread = static_cast<float>(source_files.size()) / static_cast<float>(get_thread_count(source_files));
	unsigned int remainder = source_files.size() % get_thread_count(source_files);

	// index for job_queue vector
	unsigned int index = 0;

	// holds commands to be exected for every thread
	std::vector<std::string> job_queue;

	for(unsigned int i = 0; i < get_thread_count(source_files); ++i)
	{
		if(i < (get_thread_count(source_files) - remainder))
		{
			std::string cmd_input = compiler_name + " -c";

			for(int j = 0; j < std::floor(files_per_thread); ++j)
			{
				cmd_input += " " + source_files[j + index];
			}

			for(std::string flag : compiler_flags)
			{
				cmd_input += " " + flag;
			}

			job_queue.push_back(cmd_input);

			index += std::floor(files_per_thread);
		}
		// only if remainder != 0
		// instead of floor, it ceils files_per_thread
		else
		{
			std::string cmd_input = compiler_name + " -c";

			for(int j = 0; j < std::ceil(files_per_thread); ++j)
			{
				cmd_input += " " + source_files[j + index]; 
			}

			for(std::string flag : compiler_flags)
			{
				cmd_input += " " + flag;
			}

			job_queue.push_back(cmd_input);

			index += std::ceil(files_per_thread);
		}
	}

	return job_queue;
}

inline void
link(std::string compiler_name, std::string executable_name, std::vector<std::string> linker_flags)
{
	std::string to_link;

	for(auto entry : fs::directory_iterator(get_working_path()))
	{
		if(entry.is_regular_file())
		{
			std::string mime_type = execute_silent("file -b --mime-type " + entry.path().string());

			if(mime_type == "application/x-object")
			{
				to_link += " " + entry.path().string();
			}
			
		}
	}

	std::string linker_call = compiler_name + " -o " + executable_name + to_link;

	for(std::string flag : linker_flags)
	{
		linker_call += " " + flag;
	}

	std::cout << BOLD_WHITE << "Linker command: " << RESET << linker_call << "\n";

	execute(linker_call);
}

inline void
compile(std::vector<std::thread>& thread_pool, const std::vector<std::string>& job_queue)
{
	for(unsigned int i = 0; i < thread_pool.capacity(); ++i)
	{
		std::cout << BOLD_WHITE << "Compiler command[" << i << "]: " << RESET << job_queue[i] << "\n\n";

		thread_pool.emplace_back(std::thread(&execute, job_queue[i]));
	}

	for(auto& active_thread : thread_pool)
	{
		active_thread.join();
	}
}