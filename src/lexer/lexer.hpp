#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <filesystem>
#include <sstream>

#include "../common/locate_script.hpp"
#include "../common/range_struct.hpp"
#include "../common/function_struct.hpp"
#include "../common/token.hpp"

class lexer
{
	public:
		lexer();

		void lex();

		mecha::token get_token();
		mecha::token get_current_token();

		unsigned int get_token_number();
		unsigned int get_token_index();
		unsigned int get_line_number();

		void move_token_index(int);

		void add_token(char, bool&, bool&, unsigned int&);
		void add_whitespace(mecha::token&, char, bool&, bool&);
		void add_brackets(mecha::token&, char, bool&, bool&);
		void add_quotes(mecha::token&, char, bool&, bool&);

	private:
		std::string script_string;

		std::vector<mecha::token> tokens;

		bool reached_end = false;
		bool first_token = true;

		unsigned int token_index = 0;
		unsigned int line_number = 1;
};