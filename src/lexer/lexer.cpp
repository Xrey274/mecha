#include "lexer.hpp"

lexer::lexer()
{
	/*
		Open script and load it into script_string
	*/

	std::ifstream script(get_script_path());
	
	std::stringstream buffer;
	// load script into buffer
	buffer << script.rdbuf();

	// reserve buffer based of size of script
	script_string.reserve(std::filesystem::file_size(get_script_path()));
	script_string = buffer.str();

	script.close();

	if(!script_string.length())
		error(lexer_error::empty_script);

	tokens.emplace_back(mecha::token{"", 0});
}

void
lexer::lex()
{
	bool inside_quotes = false;
	bool inside_comment = false;

	for(unsigned int i = 0; i < script_string.length() - 1; ++i)
	{
		add_token(script_string[i], inside_quotes, inside_comment, i);

		// if there are 2 empty strings in a row, remove one
		if(tokens.size() > 1 &&
		   tokens[tokens.size() - 1].string.empty() &&
		   tokens[tokens.size() - 2].string.empty())
		{
			tokens.pop_back();
		}
	}

	if(tokens.back().string.empty())
	{
		tokens.pop_back();	
	}
}

mecha::token
lexer::get_token()
{
	if(reached_end)
	{
		mecha::token token{"", tokens[token_index].line_number};

		return token;
	}
	else
	{
		if(!first_token)
			++token_index;
		else
			first_token = false;

		mecha::token token = tokens[token_index];

		if(tokens.size() - 1 == token_index && !reached_end)
			reached_end = true;

		return token;
	}
}

mecha::token
lexer::get_current_token()
{
	return tokens[token_index];
}

unsigned int
lexer::get_token_number()
{
	return tokens.size();
}

unsigned int
lexer::get_token_index()
{
	return token_index;
}

unsigned int
lexer::get_line_number()
{
	return line_number;
}

void
lexer::move_token_index(int positions)
{
	token_index += positions;
}

void
lexer::add_token(char letter, bool& inside_quotes, bool& inside_comment, unsigned int& index)
{
	mecha::token new_token;
	// need, because letter is a char
	new_token.string      = letter;
	new_token.line_number = line_number;

	if(letter == ' ' || letter == '\n' || letter == '\t')
	{
		add_whitespace(new_token, letter, inside_quotes, inside_comment);	
	}
	else if(letter == '(' || letter == ')' || letter == '{' || letter == '}' || letter == '.')
	{
		add_brackets(new_token, letter, inside_quotes, inside_comment);
	}
	else if(letter == '"')
	{
		add_quotes(new_token, letter, inside_quotes, inside_comment);
	}
	// there's two forwards slashes in a row
	else if(letter == '/' && script_string[index + 1] == '/')
	{
		index += 2;

		inside_comment = true;
	}
	else
	{
		// everything inside comments is ignored and skipped
		if(!inside_comment)
		{
			tokens.back().string      += letter;
			tokens.back().line_number = line_number;
		}
	}

	// here at the bottom as to now mess up line number order
	if(letter == '\n')
		line_number++;
}

void
lexer::add_whitespace(mecha::token& new_token, char letter, bool& inside_quotes, bool& inside_comment)
{
	if(letter != '\n' && inside_quotes)
	{
		tokens.back().string += letter;
	}
	else if (letter == '\n' && inside_quotes)
	{
		// if "closing" double quotes are not present on the same line
		inside_quotes = false;
	}
	else if(letter == '\n' && inside_comment)
	{
		inside_comment = false;

		return;
	}
	else
	{
		new_token.string = "";
		tokens.push_back(new_token);
	}
}

void
lexer::add_brackets(mecha::token& new_token, char letter, bool& inside_quotes, bool& inside_comment)
{
	if(inside_comment)
	{
		return;
	}

	if(inside_quotes)
	{
		tokens.back().string += letter;
	}
	else
	{
		if(tokens.back().string.empty())
		{
			tokens.back().string += letter;
			tokens.back().line_number = line_number;
		}
		else
		{
			tokens.push_back(new_token);
			tokens.emplace_back(mecha::token{"", line_number});
		}
	}
}

void
lexer::add_quotes(mecha::token& new_token, char letter, bool& inside_quotes, bool& inside_comment)
{
	if(inside_comment)
	{
		return;
	}

	// quotes and the stuff inside them are seperated
	if(!inside_quotes)
	{
		mecha::token quote{R"(")", line_number};

		if(tokens.back().string.empty())
		{
			tokens.back().string += letter;
		}
		else
			tokens.emplace_back(mecha::token{R"(")", line_number});

		tokens.emplace_back(mecha::token{"", line_number});

		inside_quotes = true;
	}
	else if(inside_quotes)
	{
		tokens.emplace_back(mecha::token{R"(")", line_number});

		tokens.emplace_back(mecha::token{"", line_number});

		inside_quotes = false;
	}
}