#pragma once

#include <iostream>
#include <cstdio>
#include <string>

inline std::string
execute_silent(const std::string& command) 
{ 
    FILE* stream;

    char buffer[256];
    std::string output;
    std::string to_execute = command;

    stream = popen(to_execute.c_str(), "r");

    if(stream) 
    {
	    while(fgets(buffer, 256, stream) != nullptr)
	    {
	    	output += buffer;
	    }

	    pclose(stream);
    }
    else
        throw std::string("Unable to open command stream");

    output.erase(std::remove(output.begin(), output.end(), '\n'), output.end());

    return output;
}

inline void
execute(const std::string& command)
{
	FILE* stream;

    char buffer[256];
    std::string output;
    std::string to_execute = command;

    stream = popen(to_execute.c_str(), "r");

    if(stream)
    {
	    while(fgets(buffer, 256, stream) != nullptr)
	    {
	    	output += buffer;
	    }
	    
	    pclose(stream);
    }
    else
        throw std::string("Unable to open command stream");

    std::cout << output << "\n";
}