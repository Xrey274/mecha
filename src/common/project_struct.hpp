#pragma once

#include <string>
#include <vector>

#include "function_struct.hpp"

namespace mecha
{
	struct project
	{
		std::string name;

		std::string executable_name = "executable";
		std::string compiler_name   = "g++";

		std::vector<std::string> compiler_flags;
		std::vector<std::string> source_files;
		std::vector<std::string> linker_flags;

		std::vector<function> functions;
	};
}