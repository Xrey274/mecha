#pragma once

#include <string>
#include <vector>

#include "function_list.hpp"

namespace mecha
{
	struct function
	{
		std::string name;
		functions type;
		std::vector<std::string> parameters;
	};
}