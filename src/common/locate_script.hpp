#pragma once

#include <iostream>
#include <string>
#include <filesystem>

#include "error.hpp"

namespace fs = std::filesystem;

inline std::string 
get_script_path()
{
	std::string current_path = fs::current_path();
	std::string script_path;

	for(auto entry : fs::directory_iterator(current_path))
	{
		std::string extension_type = entry.path().extension().string();

		if(entry.is_regular_file() && extension_type == ".mecha")
		{
			script_path = current_path + "/" + entry.path().filename().string();
		}
	}

	if(script_path.empty())
		error(io_error::script_not_found);

	return script_path;
}

inline std::string 
get_working_path()
{
	return fs::current_path().string();
}
