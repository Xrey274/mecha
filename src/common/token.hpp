#pragma once

#include <string>

namespace mecha
{
	struct token
	{
		std::string  string;
		unsigned int line_number;
	};
}