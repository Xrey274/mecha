#pragma once

#include <string>
#include <vector>
#include <unordered_map>

// sorted alphabetically
enum class functions
{
	add_files,
	add_files_recursive,
	default_range,
	output_to,
	pkg_config_find,
	set_compiler,
	set_compiler_flags,
	set_linker_flags,
	set_name,
	thread_info,
	move,
	copy
};

const std::unordered_map<std::string, functions> project_function_map
{
	{"add_files_recursive", functions::add_files_recursive},
	{"add_files", functions::add_files},
	{"output_to", functions::output_to},
	{"pkg_config_find", functions::pkg_config_find},
	{"set_compiler", functions::set_compiler},
	{"set_compiler_flags", functions::set_compiler_flags},
	{"set_linker_flags", functions::set_linker_flags},
	{"set_name", functions::set_name},
	{"thread_info", functions::thread_info}
};

const std::vector<std::string> project_function_vector
{
	"add_files"
	"add_files_recursive"
	"output_to"
	"pkg_config_find"
	"set_compiler"
	"set_compiler_flags"
	"set_linker_flags"
	"set_name"
	"thread_info"
};

const std::unordered_map<std::string, functions> global_function_map
{
	{"default_range", functions::default_range},
	{"move", functions::move},
	{"copy", functions::copy}
};

const std::vector<std::string> global_function_vector
{
	"default_range",
	"move",
	"copy"
};
