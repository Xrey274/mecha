#pragma once

#include <string>
#include <unordered_map>

#include "token.hpp"
#include "shell_colors.hpp"

enum class io_error
{
	script_not_found,
	compiler_not_found,
	pkg_config_not_found,
	libmagic_not_found
};

enum class lexer_error
{
	empty_script
};

enum class parser_error
{
	no_error,
	unknown_token,
	range_name_length,
	range_name_function,
	range_name_character,
	range_name_unknown,
	range_inside_range,
	range_not_opened,
	range_not_closed,
	global_function_in_range,
	project_name_length,
	project_name_function,
	project_name_character,
	project_outside_range,
	unknown_function,
	unknown_token_dot,
	function_without_project,
	function_not_opened,
	function_not_closed,
	expected_parameters,
	expected_quotes
};

enum class function_error
{

};

const std::unordered_map<io_error, std::string> io_error_map
{
	{io_error::script_not_found, " Unable to find script, terminating"},
	{io_error::compiler_not_found, " Unable to find compiler, terminating"},
	{io_error::pkg_config_not_found, " Unable to find pkg-config, terminating"},
	{io_error::libmagic_not_found, " Unable to find libmagic, terminating"}
};

const std::unordered_map<lexer_error, std::string> lexer_error_map
{
	{lexer_error::empty_script, " Empty script, terminating"}
};

const std::unordered_map<parser_error, std::string> parser_error_map
{
	{parser_error::no_error, ""},
	{parser_error::unknown_token, "Unknown token"},
	{parser_error::range_name_length, "Invalid name! Range name must be at least 3 characters long"},
	{parser_error::range_name_function, "Invalid name! Range name can't be a function"},
	{parser_error::range_name_character, "Invalid name! Range name can't contain special characters"},
	{parser_error::range_name_unknown, "Invalid range name!"},
	{parser_error::range_inside_range, "Declaring a range, inside a range is illegal!"},
	{parser_error::range_not_opened, "Range not opened, expected '{'"},
	{parser_error::range_not_closed, "Range not closed, expected '}'"},
	{parser_error::global_function_in_range, "Global function cannot be declared inside a range"},
	{parser_error::project_name_length, "Invalid name! Project name must be at least 3 characters long"},
	{parser_error::project_name_function, "Invalid name! Project name can't be a function"},
	{parser_error::project_name_character, "Iinvalid name! Project name can't contain special characters"},
	{parser_error::project_outside_range, "Invalid action, trying to declare a project outside of range"},
	{parser_error::unknown_function, "Unknown function"},
	{parser_error::unknown_token_dot, "Unknown token, expected '.'"},
	{parser_error::function_without_project, "Function cannot be used without a project"},
	{parser_error::function_not_opened, "Unknown token expected '('"},
	{parser_error::function_not_closed, "Unknown token expected ')'"},
	{parser_error::expected_parameters, "Expected parameters after double quotes"},
	{parser_error::expected_quotes, "Uknown token expected \""}
};

inline void
error(io_error error_code)
{
	std::string error = io_error_map.at(error_code);

	std::cerr << BOLD_RED << "[IO]" << error << RESET << "\n";

	exit(1);
}

inline void
error(lexer_error error_code)
{
	std::string error = lexer_error_map.at(error_code);

	std::cerr << BOLD_RED << "[LEXER]" << error << RESET << "\n";

	exit(1);
}


inline void
error(const mecha::token& token, parser_error error_code)
{
	std::string error = " " + std::to_string(token.line_number) + " | '" + token.string + "' -> " + parser_error_map.at(error_code);

	std::cerr << BOLD_RED << "[PARSER]" << error << RESET << "\n";

	exit(1);
}
