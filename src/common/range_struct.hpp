#pragma once

#include <string>
#include <vector>

#include "project_struct.hpp"

namespace mecha
{
	struct range
	{
		std::string name;
		std::vector<project> projects;
		std::vector<function> functions;
	};
}