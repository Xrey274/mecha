#include <iostream>
#include <string>
#include <fstream>
#include <thread>

#include "timer.hpp"
#include "parser/parser.hpp"
#include "common/shell_colors.hpp"

void
start(std::string range_to_execute)
{
    try
    {
        parser new_parser;

        new_parser.parse();
        new_parser.build(range_to_execute);
    }
    catch(const std::exception& e)
    {
        std::cerr << BOLD_RED << e.what() << RESET << "\n";

        return;
    }
}

int 
main(int argc, char* argv[])
{
    timer global_timer;

    global_timer.start_timer();

    std::string to_execute = (argc > 1) ? argv[1] : "";

    start(to_execute);

    global_timer.stop_timer();

    std::cout << "\n" << BOLD_WHITE << "Elapsed time: " << global_timer.get_elapsed_time() << RESET << "\n";

    return 0;
}