# README

## About the project

Mecha aims to be a simple replacement for the bloated and overly complicated build systems.

## Features

- Bare bones, function orianted, scripting launguage

- Support for C and C++

- GNU/Linux support

- No build artifacts in the form of files or directories(by default)

- Delta compilation

- Parralel compilation

- Uses pkg-config as a standardized interface for library directories and compiler flags

- Purely CLI interface, no clickity clackity GUI (https://www.youtube.com/watch?v=q5AGt17N-ik)

- Ranges

## Building

The build script is only meant for bootstrapping and to save you a few seconds of typing.

```
git clone https://gitlab.com/Xrey274/mecha.git
cd Mecha
chmod 777 build
./build
```

### Runtime dependencies:

- pkg-config
- libmagic
- g++ or similar compiler
- ld or similar linker
